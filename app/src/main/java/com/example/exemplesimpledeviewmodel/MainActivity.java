package com.example.exemplesimpledeviewmodel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Version de départ sans ViewModel
 * Problème : quand on tourne l'écran, l'activité est recréée et on perd la valeur courante du compteur.
 * Solution 2 : on utilise onPause et onResume (solution conseillée pour garder des données, qui
 * intègre tous les cas)
 */

public class MainActivity extends AppCompatActivity {

    private int compteur;
    private Button boutonPlus, boutonMoins;
    private TextView txt_compteur;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_compteur = findViewById(R.id.compteur);

        // boutons + et -
        boutonMoins = findViewById(R.id.btn_moins);
        boutonMoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compteur--;
                miseAJourIHM(compteur);
            }
        });
        boutonPlus = findViewById(R.id.btn_plus);
        boutonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compteur++;
                miseAJourIHM(compteur);
            }
        });

        // valeur abitraire de départ pour vérifier ce qui se passe en cas de rotation
        compteur = 10;
        miseAJourIHM(compteur);
    }

    private void miseAJourIHM(int compteur) {
        txt_compteur.setText("" + compteur);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Intent intent = getIntent();
        intent.putExtra("compteur", compteur);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // s'il n'y a pas de valeur sauvegardée, alors on met la valeur arvbitraire de 100.
        compteur=getIntent().getIntExtra("compteur",111);
        miseAJourIHM(compteur);
    }
}